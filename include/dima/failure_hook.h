/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * dima/failure_hook.h
 * -------------------
 * A DIMA implementation that invokes a user-defined function on failure.
 *
 * Although, the functionality of this DIMA can easily be implemented with a
 * proxy. However, this implementation can be assumed to be more performant
 * because it avoids calls through function pointers in the success path.
 *
 * The dima_new_failure_hook() function creates a new failure hook, which can be
 * used wherever a dima_t * is needed, by using the dima_from_failure_hook()
 * function.
 *
 * The implementation is thread safe if the implementation passed as next
 * parameter to dima_new_failure_hook() is.
 *
 * There is no public failure hook type. Instead, each failure hook is
 * identified by its userdata pointer. This makes the user code more concise and
 * reduces the number of API functions. The drawback is a slight loss of type
 * safety.
 */

#ifndef DIMA_FAILURE_HOOK_H
#define DIMA_FAILURE_HOOK_H

#include <dima/dima.h>

typedef void dima_on_failure_fn(void *userdata);

/**
 * Creates a new failure hook that calls the on_failure function on failure.
 *
 * The hook is allocated using the next DIMA. When the hook is no longer needed,
 * its corresponding DIMA (see dima_from_failure_hook) shall be passed to
 * dima_unref.
 *
 * The newly created failure hook is uniquely identified by its userdata, whose
 * size is fixed during the lifetime of the hook and is given by the size
 * parameter.
 *
 * @param next the next DIMA
 * @param on_failure the function to call on failure
 * @param flags the flags
 * @param size the size of the userdata
 * @return a pointer to the userdata of the hook, or NULL on failure
 */
void *dima_new_failure_hook(dima_t *next,
                            dima_on_failure_fn *on_failure,
                            unsigned flags,
                            size_t size);

/**
 * Returns the DIMA implementation of the failure hook given a pointer to its
 * userdata.
 */
dima_t *dima_from_failure_hook(void *userdata);

#endif /* !DIMA_FAILURE_HOOK_H */
