/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * dima/synchronizer.h
 * -------------------
 * A DIMA proxy that forwards to another implementation while synchronizing on a
 * mutex.
 *
 * If the next implementation is not thread hostile, then this implementation is
 * thread-safe. If the next implementation is thread-hostile, the behavior is
 * undefined.
 */

#ifndef DIMA_SYNCHRONIZER_H
#define DIMA_SYNCHRONIZER_H

#include <dima/dima.h>

typedef struct dima_synchronizer_s dima_synchronizer_t;

/**
 * Initializes the given DIMA to forward to the next implementation while
 * synchronizing on a mutex.
 *
 * The initialized DIMA is thread-safe. If the next DIMA is thread-hostile, the
 * behavior is undefined.
 *
 * The synchronizer is allocated using the next DIMA. When the synchronizer is
 * no longer needed, it shall either be passed to dima_unref_synchronizer, or
 * its corresponding DIMA (see dima_from_synchronizer) shall be passed to
 * dima_unref.
 *
 * @param next the next DIMA implementation
 * @return the synchronizer, or NULL if it could not be created, for example due
 *         to memory allocation failure
 */
dima_synchronizer_t *dima_new_synchronizer(dima_t *next);

/**
 * Decrements the reference count of the synchronizer.
 *
 * If the synchronizer is NULL, this function does nothing.
 *
 * @param synchronizer the synchronizer, or NULL
 */
void dima_unref_synchronizer(dima_synchronizer_t *synchronizer);

/**
 * Returns the synchronizer's DIMA implementation.
 */
dima_t *dima_from_synchronizer(dima_synchronizer_t *synchronizer);

#endif /* !DIMA_SYNCHRONIZER_H */
