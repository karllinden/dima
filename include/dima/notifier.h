/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * dima/notifier.h
 * ---------------
 * A DIMA that notifies a list of listeners when it is invoked.
 *
 * This implementation is not thread-safe, but is not thread hostile, so it can
 * be made thread safe by adding synchronization.
 */

#ifndef DIMA_NOTIFIER_H
#define DIMA_NOTIFIER_H

#include <dima/invocation.h>

typedef struct dima_notifier_s dima_notifier_t;
typedef struct dima_listener_s dima_listener_t;

typedef void dima_notify_fn(dima_listener_t *listener,
                            const struct dima_invocation *invocation,
                            void *result);

/**
 * Creates a DIMA implementation that forwards to the given next DIMA, and
 * notifies listeners about invocations.
 *
 * The notifier is allocated using the next DIMA. When the notifier is no longer
 * needed, it shall either be passed to dima_unref_notifier, or its
 * corresponding DIMA (see dima_from_notifier) shall be passed to dima_unref.
 *
 * @param next the DIMA to forward to
 * @return the newly created notifier, or NULL on failure
 */
dima_notifier_t *dima_new_notifier(dima_t *next);

/**
 * Decrements the reference count of the notifier.
 *
 * If the notifier is NULL, this function does nothing.
 *
 * @param notifier the notifier, or NULL
 */
void dima_unref_notifier(dima_notifier_t *notifier);

/**
 * Returns the notifier's DIMA implementation.
 */
dima_t *dima_from_notifier(dima_notifier_t *notifier);

/**
 * Creates a listener that listens for notifications from the given notifier.
 *
 * The listener is allocated with the next DIMA of the notifier. When the
 * listener is no longer needed, it shall be passed to dima_destroy_listener.
 *
 * The notification function may create and destroy listeners on the notifier,
 * but may not invoke the notifier through any of the generic DIMA functions
 * (such as dima_free, dima_alloc and dima_strdup).
 *
 * @param notifier the notifier to listen to
 * @param notify the notification function that the notifier should call with
 *               the listener
 * @param userdata_size the size of the userdata to allocate together with the
 *                      listener
 * @return the listener, or NULL if memory allocation fails
 */
dima_listener_t *dima_new_listener(dima_notifier_t *notifier,
                                   dima_notify_fn *notify,
                                   size_t userdata_size);

/**
 * Destroys the given listener.
 *
 * If the listener is NULL, this function does nothing.
 *
 * @param listener the listener to destroy, or NULL
 */
void dima_destroy_listener(dima_listener_t *listener);

/**
 * Returns the userdata of the given listener.
 *
 * @param listener the listener
 * @return the userdata
 */
void *dima_listener_userdata(dima_listener_t *listener);

#endif /* !DIMA_NOTIFIER_H */
