/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * dima/countdown.h
 * ----------------
 * A DIMA that fails after a given number of allocations.
 *
 * A countdown wraps a DIMA and, when enabled, introduces failure after a given
 * number of allocations. The allocation count can be zero, in which case the
 * countdown always fails. Because countdowns can be disabled, they can be used
 * to test units that perform some initial allocations that must succeed.
 *
 * A disabled countdown always invokes the next DIMA and does not count down. An
 * enabled countdown introduces failure when the count has reached zero.
 * Otherwise it counts down and invokes the next DIMA.
 *
 * This implementation is not thread-safe, but is not thread hostile, so it can
 * be made thread safe by adding synchronization.
 */

#ifndef DIMA_COUNTDOWN_H
#define DIMA_COUNTDOWN_H

#include <dima/dima.h>

typedef struct dima_countdown_s dima_countdown_t;

/**
 * Creates a new DIMA that introduces failure after a number of allocations.
 *
 * Initially, the countdown is enabled and fails directly (i.e. it fails after 0
 * allocations). The countdown can be disabled by passing it to
 * dima_disable_countdown. The number of allocations to forward can be modified
 * with dima_start_countdown.
 *
 * The countdown is allocated using the next DIMA. When the countdown is no
 * longer needed, it shall either be passed to dima_unref_countdown, or its
 * corresponding DIMA (see dima_from_countdown) shall be passed to dima_unref.
 *
 * @param next the DIMA to forward to
 * @return the countdown, or NULL if memory allocation fails
 */
dima_countdown_t *dima_new_countdown(dima_t *next);

/**
 * Decrements the reference count of the countdown.
 *
 * If the countdown is NULL, this function does nothing.
 *
 * @param countdown the countdownn, or NULL
 */
void dima_unref_countdown(dima_countdown_t *countdown);

/**
 * Returns the countdown's DIMA implementation.
 */
dima_t *dima_from_countdown(dima_countdown_t *countdown);

/**
 * Makes the countdown fail after the given number of allocations.
 *
 * After a call to this function, the countdown forwards n_allocs to the
 * forwardee and introduces failure afterwards. This function does not modify
 * the enablement of the countdown.
 *
 * @param countdown the countdown to modify
 * @param n_allocs the number allocations to forward
 */
void dima_start_countdown(dima_countdown_t *countdown, unsigned n_allocs);

/**
 * Enables the given countdown.
 */
void dima_enable_countdown(dima_countdown_t *countdown);

/**
 * Disables the given countdown.
 */
void dima_disable_countdown(dima_countdown_t *countdown);

#endif /* !DIMA_COUNTDOWN_H */
