/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * dima/exiting_on_failure.h
 * -------------------------
 * A DIMA implementation that wraps another DIMA and exits with a given status
 * on failure.
 *
 * The implementation is thread safe if the implementation passed as next
 * parameter to dima_new_exiting_on_failure() is.
 */

#ifndef DIMA_EXITING_ON_FAILURE_H
#define DIMA_EXITING_ON_FAILURE_H

#include <dima/dima.h>

/**
 * Creates a DIMA that forwards to the next DIMA and exits on failure.
 *
 * The DIMA is allocated with the next DIMA. When the DIMA is no longer needed,
 * it shall be passed to dima_unref.
 *
 * @param next the next DIMA
 * @param status the exit status
 * @return the DIMA that exits on failure, or NULL on failure
 */
dima_t *dima_new_exiting_on_failure(dima_t *next, int status);

#endif /* !DIMA_EXITING_ON_FAILURE_H */
