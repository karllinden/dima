/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * dima/random.h
 * -------------
 * A DIMA proxy that fails randomly with a given rate.
 *
 * For portability, this implementation is based on rand(3) and is therefore
 * thread hostile. If you need a thread-safe implementation, or an
 * implementation with higher quality randomness, consider writing a custom
 * dima_proxy implementation.
 *
 * Before using this implementation you should seed the random number generator
 * with srand(3).
 */

#ifndef DIMA_RANDOM_H
#define DIMA_RANDOM_H

#include <dima/proxy.h>

typedef struct dima_random_s dima_random_t;

/**
 * Creates a new DIMA that forwards to the next DIMA and fails randomly with a
 * given percentage.
 *
 * Initially, the failure percentage is 0. To set the failure percentage use
 * dima_set_failure_percentage.
 *
 * The random is allocated using the next DIMA. When the random is no longer
 * needed, it shall either be passed to dima_unref_random, or its corresponding
 * DIMA (see dima_from_random) shall be passed to dima_unref.
 *
 * @param next the DIMA to forward to
 * @return the random DIMA, or NULL if memory allocation fails
 */
dima_random_t *dima_new_random(dima_t *next);

/**
 * Decrements the reference count of the random.
 *
 * If the random is NULL, this function does nothing.
 *
 * @param random the random, or NULL
 */
void dima_unref_random(dima_random_t *random);

/**
 * Returns the random's DIMA implementation.
 */
dima_t *dima_from_random(dima_random_t *random);

/**
 * Sets the failure percentage of the given random DIMA.
 *
 * A failure_percentage of 0 causes the dima to never fail. On the other
 * extreme, a failure_percentage of 100 causes the dima to always fail. Calling
 * this function with a failure_percentage that is less than 0 or greater than
 * 100 invokes undefined behavior.
 *
 * @param random the random DIMA to modify
 * @param failure_percentage the failure percentage
 */
void dima_set_failure_percentage(dima_random_t *random, int failure_percentage);

#endif /* !DIMA_RANDOM_H */
