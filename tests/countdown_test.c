/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dima/countdown.h>
#include <dima/exiting_on_failure.h>
#include <dima/system.h>

#include "test.h"

static dima_countdown_t *countdown;
static dima_t *dima_under_test;

void set_up(void) {
    countdown = dima_new_countdown(dima_system_instance());
    dima_under_test = dima_from_countdown(countdown);
}

void tear_down(void) {
    dima_under_test = NULL;
    dima_unref_countdown(countdown);
    countdown = NULL;
}

START_TEST(does_not_exit_on_failure) {
    ck_assert_int_eq(0, dima_exits_on_failure(dima_under_test));
}
END_TEST

START_TEST(does_not_exit_on_failure_even_if_next_does) {
    dima_t *exiting = dima_new_exiting_on_failure(dima_system_instance(), 28);
    dima_countdown_t *c = dima_new_countdown(exiting);
    ck_assert_int_eq(0, dima_exits_on_failure(dima_from_countdown(c)));
    dima_unref_countdown(c);
    dima_unref(exiting);
}
END_TEST

START_TEST(is_not_thread_safe_even_if_next_is) {
    ck_assert_int_eq(0, dima_is_thread_safe(dima_under_test));
}
END_TEST

START_TEST(alloc_fails_after_0) {
    void *ptr = dima_alloc(dima_under_test, 8);
    ck_assert_ptr_eq(NULL, ptr);
}
END_TEST

START_TEST(alloc0_fails_after_1) {
    dima_start_countdown(countdown, 1);
    void *ptr1 = dima_alloc0(dima_under_test, 16);
    void *ptr2 = dima_alloc0(dima_under_test, 16);
    ck_assert_ptr_ne(NULL, ptr1);
    ck_assert_ptr_eq(NULL, ptr2);
    dima_free(dima_under_test, ptr1);
}
END_TEST

START_TEST(consistently_fails_after_countdown) {
    dima_start_countdown(countdown, 1);
    void *ptr1 = dima_alloc(dima_under_test, 16);
    ck_assert_ptr_ne(NULL, ptr1);

    void *ptr2 = dima_realloc(dima_under_test, ptr1, 32);
    ck_assert_ptr_eq(NULL, ptr2);

    void *ptr3 = dima_realloc(dima_under_test, ptr1, 32);
    ck_assert_ptr_eq(NULL, ptr3);

    dima_free(dima_under_test, ptr1);
}
END_TEST

START_TEST(free_does_not_count) {
    dima_start_countdown(countdown, 2);
    void *ptr1 = dima_alloc_array(dima_under_test, 4, 32);
    ck_assert_ptr_ne(NULL, ptr1);
    dima_free(dima_under_test, ptr1);

    void *ptr2 = dima_alloc_array0(dima_under_test, 2, 16);
    ck_assert_ptr_ne(NULL, ptr2);
    dima_free(dima_under_test, ptr2);
}
END_TEST

START_TEST(second_realloc_fails) {
    dima_start_countdown(countdown, 2);
    void *ptr1 = dima_alloc_array(dima_under_test, 4, 32);
    ck_assert_ptr_ne(NULL, ptr1);

    void *ptr2 = dima_realloc_array(dima_under_test, ptr1, 8, 32);
    ck_assert_ptr_ne(NULL, ptr2);

    void *ptr3 = dima_realloc_array(dima_under_test, ptr2, 16, 32);
    ck_assert_ptr_eq(NULL, ptr3);
    dima_free(dima_under_test, ptr2);
}
END_TEST

START_TEST(unref_null_works) {
    dima_unref_countdown(NULL);
}
END_TEST

START_TEST(new_countdown_fails_if_allocation_fails) {
    dima_countdown_t *cd = dima_new_countdown(dima_from_countdown(countdown));
    ck_assert_ptr_eq(NULL, cd);
}
END_TEST

START_TEST(countdown_owns_next_dima) {
    dima_countdown_t *a = dima_new_countdown(dima_system_instance());
    dima_start_countdown(a, 1);

    dima_countdown_t *b = dima_new_countdown(dima_from_countdown(a));
    dima_start_countdown(b, 1);
    dima_unref_countdown(a);
    a = NULL;

    dima_t *d = dima_from_countdown(b);
    char *s = dima_strdup(d, "hello");
    dima_free(d, s);

    dima_unref_countdown(b);
}
END_TEST

START_TEST(disabled_countdown_does_not_fail) {
    dima_disable_countdown(countdown);
    void *ptr = dima_alloc(dima_under_test, 8);
    ck_assert_ptr_ne(NULL, ptr);

    dima_free(dima_under_test, ptr);
}
END_TEST

START_TEST(reenabled_countdown_fails) {
    dima_disable_countdown(countdown);
    dima_enable_countdown(countdown);
    char *s = dima_strdup(dima_under_test, "ABC");
    ck_assert_ptr_eq(NULL, s);
}
END_TEST

START_TEST(reenabled_countdown_continues) {
    dima_start_countdown(countdown, 1);
    dima_disable_countdown(countdown);
    dima_enable_countdown(countdown);
    char *ptr1 = dima_alloc(dima_under_test, 32);
    char *ptr2 = dima_alloc(dima_under_test, 32);
    ck_assert_ptr_ne(NULL, ptr1);
    ck_assert_ptr_eq(NULL, ptr2);
    dima_free(dima_under_test, ptr1);
    dima_free(dima_under_test, ptr2);
}
END_TEST

void add_tests(void) {
    ADD_TEST(does_not_exit_on_failure);
    ADD_TEST(does_not_exit_on_failure_even_if_next_does);
    ADD_TEST(is_not_thread_safe_even_if_next_is);
    ADD_TEST(alloc_fails_after_0);
    ADD_TEST(alloc0_fails_after_1);
    ADD_TEST(consistently_fails_after_countdown);
    ADD_TEST(free_does_not_count);
    ADD_TEST(second_realloc_fails);
    ADD_TEST(unref_null_works);
    ADD_TEST(new_countdown_fails_if_allocation_fails);
    ADD_TEST(countdown_owns_next_dima);
    ADD_TEST(disabled_countdown_does_not_fail);
    ADD_TEST(reenabled_countdown_fails);
    ADD_TEST(reenabled_countdown_continues);
}
