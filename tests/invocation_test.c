/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dima/invocation.h>

#include "test.h"

static struct dima_invocation alloc_20;
static struct dima_invocation alloc_40;
static struct dima_invocation alloc0_20;

void set_up(void) {
    dima_init_alloc_invocation(&alloc_20, 20);
    dima_init_alloc_invocation(&alloc_40, 40);
    dima_init_alloc0_invocation(&alloc0_20, 20);
}

void tear_down(void) {
    /* empty */
}

START_TEST(alloc_and_alloc0_are_different_invocations) {
    ck_assert_int_ne(0, dima_compare_invocations(&alloc_20, &alloc0_20));
}
END_TEST

START_TEST(compare_in_opposite_order_returns_additive_inverse) {
    int a = dima_compare_invocations(&alloc_20, &alloc0_20);
    int b = dima_compare_invocations(&alloc0_20, &alloc_20);
    ck_assert_int_eq(a, -b);
}
END_TEST

START_TEST(alloc_invocations_with_different_size_compare_unequal) {
    ck_assert_int_ne(0, dima_compare_invocations(&alloc_20, &alloc_40));
}
END_TEST

void add_tests(void) {
    ADD_TEST(alloc_and_alloc0_are_different_invocations);
    ADD_TEST(compare_in_opposite_order_returns_additive_inverse);
    ADD_TEST(alloc_invocations_with_different_size_compare_unequal);
}
