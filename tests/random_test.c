/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <time.h>

#include <dima/countdown.h>
#include <dima/exiting_on_failure.h>
#include <dima/random.h>
#include <dima/system.h>

#include "test.h"

static dima_random_t *random_dima;
static dima_t *dima_under_test;

START_TEST(does_not_exit_on_failure) {
    ck_assert_int_eq(0, dima_exits_on_failure(dima_under_test));
}
END_TEST

START_TEST(does_not_exit_on_failure_even_if_next_does) {
    dima_t *exiting = dima_new_exiting_on_failure(dima_system_instance(), 28);
    dima_random_t *r = dima_new_random(exiting);
    ck_assert_int_eq(0, dima_exits_on_failure(dima_from_random(r)));
    dima_unref_random(r);
    dima_unref(exiting);
}
END_TEST

START_TEST(is_not_thread_safe) {
    ck_assert_int_eq(0, dima_is_thread_safe(dima_under_test));
}
END_TEST

START_TEST(is_thread_hostile) {
    ck_assert_int_ne(0, dima_is_thread_hostile(dima_under_test));
}
END_TEST

START_TEST(new_random_returns_null_if_allocation_fails) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_random_t *random = dima_new_random(dima_from_countdown(countdown));
    ck_assert_ptr_eq(NULL, random);
    dima_unref_countdown(countdown);
}
END_TEST

START_TEST(random_owns_next_dima) {
    dima_countdown_t *countdown = dima_new_countdown(dima_system_instance());
    dima_disable_countdown(countdown);
    dima_random_t *random = dima_new_random(dima_from_countdown(countdown));
    dima_unref_countdown(countdown);

    dima_t *dima = dima_from_random(random);
    void *ptr = dima_alloc(dima, 70);
    ck_assert_ptr_ne(NULL, ptr);
    dima_free(dima, ptr);

    dima_unref_random(random);
}
END_TEST

static int count_failures(void) {
    int result = 0;
    for (int i = 0; i < 100; ++i) {
        void *ptr = dima_alloc(dima_under_test, 16);
        result += (ptr == NULL);
        dima_free(dima_under_test, ptr);
    }
    return result;
}

START_TEST(never_fails_if_failure_percentage_is_0) {
    int failure_count = count_failures();
    ck_assert_int_eq(0, failure_count);
}
END_TEST

START_TEST(always_fails_if_failure_percentage_is_100) {
    dima_set_failure_percentage(random_dima, 100);
    int failure_count = count_failures();
    ck_assert_int_eq(100, failure_count);
}
END_TEST

START_TEST(has_ok_failure_count_if_failure_percentage_is_50) {
    dima_set_failure_percentage(random_dima, 50);
    int failure_count = count_failures();

    /* The probability for failure_count <= 7 || failure_count >= 93 is
     * approximately 2.72 * 10^-20, and that probability is so vanishingly
     * small that we deem it impossible. */
    ck_assert_int_gt(failure_count, 7);
    ck_assert_int_lt(failure_count, 93);
}
END_TEST

void set_up(void) {
    srand(time(NULL));
    random_dima = dima_new_random(dima_system_instance());
    dima_under_test = dima_from_random(random_dima);
}

void tear_down(void) {
    dima_under_test = NULL;
    dima_unref_random(random_dima);
    random_dima = NULL;
}

void add_tests(void) {
    ADD_TEST(does_not_exit_on_failure);
    ADD_TEST(does_not_exit_on_failure_even_if_next_does);
    ADD_TEST(is_not_thread_safe);
    ADD_TEST(is_thread_hostile);
    ADD_TEST(new_random_returns_null_if_allocation_fails);
    ADD_TEST(random_owns_next_dima);
    ADD_TEST(never_fails_if_failure_percentage_is_0);
    ADD_TEST(always_fails_if_failure_percentage_is_100);
    ADD_TEST(has_ok_failure_count_if_failure_percentage_is_50);
}
