/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>

#include "test_listener.h"

static void notify_test_listener(dima_listener_t *listener,
                                 const struct dima_invocation *invocation,
                                 void *result) {
    struct test_listener_data *data = dima_listener_userdata(listener);
    data->count++;
    dima_copy_invocation(&data->invocation, invocation);
    data->result = result;
}

void clear_test_listener_data(struct test_listener_data *data) {
    memset(data, 0, sizeof(*data));
}

dima_listener_t *new_test_listener(dima_notifier_t *notifier) {
    dima_listener_t *listener = dima_new_listener(
            notifier, notify_test_listener, sizeof(struct test_listener_data));
    struct test_listener_data *data = dima_listener_userdata(listener);
    clear_test_listener_data(data);
    return listener;
}
