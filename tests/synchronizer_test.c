/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dima/countdown.h>
#include <dima/exiting_on_failure.h>
#include <dima/synchronizer.h>
#include <dima/system.h>

#include "mutex_tests.h"
#include "race.h"
#include "test.h"

static dima_countdown_t *countdown;
static dima_synchronizer_t *synchronizer;
static dima_t *dima_under_test;

static dima_synchronizer_t *create_synchronizer(void) {
    return dima_new_synchronizer(dima_from_countdown(countdown));
}

void set_up(void) {
    set_up_mutex_tests();

    countdown = dima_new_countdown(dima_system_instance());
    dima_disable_countdown(countdown);

    synchronizer = create_synchronizer();
    dima_under_test = dima_from_synchronizer(synchronizer);
}

static void tear_down_countdown(void) {
    dima_unref_countdown(countdown);
    countdown = NULL;
}

void tear_down(void) {
    dima_under_test = NULL;

    dima_unref_synchronizer(synchronizer);
    synchronizer = NULL;
    tear_down_countdown();

    tear_down_mutex_tests();
}

START_TEST(does_not_exit_on_failure_if_next_does_not) {
    ck_assert_int_eq(0, dima_exits_on_failure(dima_under_test));
}
END_TEST

START_TEST(exits_on_failure_if_next_does) {
    dima_t *exiting = dima_new_exiting_on_failure(dima_system_instance(), 27);
    dima_synchronizer_t *s = dima_new_synchronizer(exiting);
    ck_assert_int_eq(1, dima_exits_on_failure(dima_from_synchronizer(s)));
    dima_unref_synchronizer(s);
    dima_unref(exiting);
}
END_TEST

START_TEST(is_thread_safe) {
    ck_assert_int_ne(0, dima_is_thread_safe(dima_under_test));
}
END_TEST

START_TEST(new_synchronizer_returns_null_if_allocation_fails) {
    dima_enable_countdown(countdown);
    dima_synchronizer_t *s = create_synchronizer();
    ck_assert_ptr_eq(NULL, s);
}
END_TEST

START_TEST(new_synchronizer_returns_null_if_mutex_init_fails_after_0) {
    fail_pthread_mutex_init_after(0);
    dima_synchronizer_t *s = create_synchronizer();
    ck_assert_ptr_eq(NULL, s);
}
END_TEST

START_TEST(new_synchronizer_returns_null_if_mutex_init_fails_after_1) {
    fail_pthread_mutex_init_after(1);
    dima_synchronizer_t *s = create_synchronizer();
    ck_assert_ptr_eq(NULL, s);
}
END_TEST

START_TEST(new_synchronizer_succeeds_if_mutex_init_fails_after_2) {
    fail_pthread_mutex_init_after(2);
    dima_synchronizer_t *s = create_synchronizer();
    ck_assert_ptr_ne(NULL, s);
    dima_unref_synchronizer(s);
}
END_TEST

START_TEST(synchronizer_owns_next_dima) {
    tear_down_countdown();

    void *ptr = dima_alloc(dima_under_test, 70);
    ck_assert_ptr_ne(NULL, ptr);
    dima_free(dima_under_test, ptr);
}
END_TEST

static void alloc_and_free(void) {
    void *ptr = dima_alloc(dima_under_test, 28);
    dima_free(dima_under_test, ptr);
}

START_TEST(alloc_and_free_are_race_free) {
    dima_enable_countdown(countdown);
    dima_start_countdown(countdown, 500);
    race_with_defaults(alloc_and_free);
}
END_TEST

void add_tests(void) {
    ADD_TEST(does_not_exit_on_failure_if_next_does_not);
    ADD_TEST(exits_on_failure_if_next_does);
    ADD_TEST(is_thread_safe);
    ADD_TEST(new_synchronizer_returns_null_if_allocation_fails);
    ADD_TEST(new_synchronizer_returns_null_if_mutex_init_fails_after_0);
    ADD_TEST(new_synchronizer_returns_null_if_mutex_init_fails_after_1);
    ADD_TEST(new_synchronizer_succeeds_if_mutex_init_fails_after_2);
    ADD_TEST(synchronizer_owns_next_dima);
    ADD_TEST(alloc_and_free_are_race_free);
}
