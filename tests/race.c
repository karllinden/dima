/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "race.h"

struct race_track {
    unsigned n_racers;
    unsigned n_laps;
    race_run_lap_fn *run_lap;

    unsigned n_ready_racers;
    int go;

    pthread_mutex_t mutex;
    pthread_cond_t racer_ready;
    pthread_cond_t start_racers;
    pthread_t *racers;
};

static void handle_pthread_result(const char *function, int result) {
    if (result) {
        fprintf(stderr, "%s: %d\n", function, result);
        fflush(stderr);
        abort();
    }
}

static void alloc_racers(struct race_track *track) {
    track->racers = calloc(track->n_racers, sizeof(*track->racers));
    if (track->racers == NULL) {
        perror("calloc");
        abort();
    }
}

static void init_race_track(struct race_track *track,
                            unsigned n_racers,
                            unsigned n_laps,
                            race_run_lap_fn *run_lap) {
    track->n_racers = n_racers;
    track->n_laps = n_laps;
    track->run_lap = run_lap;
    track->n_ready_racers = 0;
    track->go = 0;

    int result = pthread_mutex_init(&track->mutex, NULL);
    handle_pthread_result("pthread_mutex_init", result);

    result = pthread_cond_init(&track->racer_ready, NULL);
    handle_pthread_result("pthread_cond_init", result);

    result = pthread_cond_init(&track->start_racers, NULL);
    handle_pthread_result("pthread_cond_init", result);

    alloc_racers(track);
}

static void deinit_race_track(struct race_track *track) {
    free(track->racers);
    pthread_cond_destroy(&track->start_racers);
    pthread_cond_destroy(&track->racer_ready);
    pthread_mutex_destroy(&track->mutex);
}

static void tell_racer_is_ready(struct race_track *track) {
    pthread_mutex_lock(&track->mutex);
    track->n_ready_racers++;
    pthread_cond_signal(&track->racer_ready);
    pthread_mutex_unlock(&track->mutex);
}

static void wait_for_go(struct race_track *track) {
    pthread_mutex_lock(&track->mutex);
    while (!track->go) {
        pthread_cond_wait(&track->start_racers, &track->mutex);
    }
    pthread_mutex_unlock(&track->mutex);
}

static void run_laps(struct race_track *track) {
    for (unsigned i = 0; i < track->n_laps; ++i) {
        track->run_lap();
    }
}

static void *racer_routine(void *ptr) {
    struct race_track *track = ptr;
    tell_racer_is_ready(track);
    wait_for_go(track);
    run_laps(track);
    return NULL;
}

static void create_racers(struct race_track *track) {
    for (unsigned i = 0; i < track->n_racers; ++i) {
        int result
                = pthread_create(track->racers + i, NULL, racer_routine, track);
        handle_pthread_result("pthread_create", result);
    }
}

static void wait_until_racers_are_ready(struct race_track *track) {
    pthread_mutex_lock(&track->mutex);
    while (track->n_ready_racers < track->n_racers) {
        pthread_cond_wait(&track->racer_ready, &track->mutex);
    }
    pthread_mutex_unlock(&track->mutex);
}

static void start_racers(struct race_track *track) {
    pthread_mutex_lock(&track->mutex);
    track->go = 1;
    pthread_cond_broadcast(&track->start_racers);
    pthread_mutex_unlock(&track->mutex);
}

static void join_racers(struct race_track *track) {
    for (unsigned i = 0; i < track->n_racers; ++i) {
        pthread_join(track->racers[i], NULL);
    }
}

void race(unsigned n_racers, unsigned n_laps, race_run_lap_fn *run_lap) {
    struct race_track track;
    init_race_track(&track, n_racers, n_laps, run_lap);
    create_racers(&track);
    wait_until_racers_are_ready(&track);
    start_racers(&track);
    join_racers(&track);
    deinit_race_track(&track);
}

void race_with_defaults(race_run_lap_fn *run_lap) {
    race(8, 100, run_lap);
}
