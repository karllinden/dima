/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <check.h>

#include "src/mutex.h"

#include "mutex_tests.h"

static int fail_after;
static unsigned count;

static int test_mutex_init(pthread_mutex_t *mutex,
                           const pthread_mutexattr_t *attr) {
    if (fail_after == 0) {
        return 1;
    } else if (fail_after > 0) {
        fail_after--;
    }

    count++;
    return pthread_mutex_init(mutex, attr);
}

static int test_mutex_destroy(pthread_mutex_t *mutex) {
    count--;
    return pthread_mutex_destroy(mutex);
}

void set_up_mutex_tests(void) {
    fail_after = -1;
    count = 0;
    dima_pthread_mutex_init = test_mutex_init;
    dima_pthread_mutex_destroy = test_mutex_destroy;
}

void tear_down_mutex_tests(void) {
    dima_pthread_mutex_init = pthread_mutex_init;
    dima_pthread_mutex_destroy = pthread_mutex_destroy;
    ck_assert_msg(count == 0,
                  "There is a mismatch between dima_pthread_mutex_init and "
                  "dima_pthread_mutex_destroy.");
}

void fail_pthread_mutex_init_after(int count) {
    fail_after = count;
}
