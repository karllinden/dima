/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ecb.h>

#include <dima/failure_hook.h>

#include "forwarding_tests.h"

static void my_hook(ecb_unused void *userdata) {
    /* noop */
}

dima_t *create_forwarding_dima(dima_t *forwardee) {
    void *hook = dima_new_failure_hook(
            forwardee, my_hook, dima_flags(forwardee), 0);
    return dima_from_failure_hook(hook);
}
