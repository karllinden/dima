/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <stdlib.h>

#include <ecb.h>

#include <dima/random.h>

#define LIMIT (RAND_MAX - RAND_MAX % 100)

struct dima_random_s {
    int failure_percentage;
};

/* Returns a random integer in the range [0,100). */
static int rand100(void) {
    int r;
    do {
        r = rand();
    } while (r >= LIMIT);
    return r % 100;
}

static int should_succeed(const dima_random_t *d) {
    int r = rand100();
    return r >= d->failure_percentage;
}

static void *invoke_random(dima_t *next,
                           void *userdata,
                           const struct dima_invocation *invocation) {
    dima_random_t *random = userdata;
    if (invocation->function == DIMA_FREE || should_succeed(random)) {
        return dima_invoke(next, invocation);
    } else {
        return NULL;
    }
}

dima_random_t *dima_new_random(dima_t *next) {
    unsigned flags = dima_flags(next);
    flags &= ~DIMA_EXITS_ON_FAILURE;
    flags &= ~DIMA_IS_THREAD_SAFE;
    flags |= DIMA_IS_THREAD_HOSTILE;

    dima_random_t *random
            = dima_new_proxy(next, invoke_random, flags, sizeof(*random));
    if (ecb_expect_false(random == NULL)) {
        return NULL;
    }

    random->failure_percentage = 0;
    return random;
}

void dima_unref_random(dima_random_t *random) {
    dima_unref_proxy(random);
}

dima_t *dima_from_random(dima_random_t *random) {
    return dima_from_proxy(random);
}

void dima_set_failure_percentage(dima_random_t *random,
                                 int failure_percentage) {
    random->failure_percentage = failure_percentage;
}
