/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DIMA_MUTEX_H
#define DIMA_MUTEX_H

#include <pthread.h>

typedef int dima_pthread_mutex_init_fn(pthread_mutex_t *mutex,
                                       const pthread_mutexattr_t *attr);
typedef int dima_pthread_mutex_destroy_fn(pthread_mutex_t *mutex);

extern dima_pthread_mutex_init_fn *dima_pthread_mutex_init;
extern dima_pthread_mutex_destroy_fn *dima_pthread_mutex_destroy;

#endif /* !DIMA_MUTEX_H */
