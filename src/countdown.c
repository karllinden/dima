/*
 * Copyright (C) 2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdbool.h>

#include <ecb.h>

#include <dima/countdown.h>
#include <dima/proxy.h>

struct dima_countdown_s {
    bool disabled;
    unsigned n_allocs;
};

static bool is_success(dima_countdown_t *countdown,
                       const struct dima_invocation *invocation) {
    if (invocation->function == DIMA_FREE || countdown->disabled) {
        return true;
    } else if (countdown->n_allocs == 0) {
        return false;
    } else {
        countdown->n_allocs--;
        return true;
    }
}

static void *invoke_countdown(dima_t *next,
                              void *userdata,
                              const struct dima_invocation *invocation) {
    dima_countdown_t *countdown = userdata;
    if (is_success(countdown, invocation)) {
        return dima_invoke(next, invocation);
    } else {
        return NULL;
    }
}

dima_countdown_t *dima_new_countdown(dima_t *next) {
    unsigned flags = dima_flags(next);
    flags &= ~DIMA_EXITS_ON_FAILURE;
    flags &= ~DIMA_IS_THREAD_SAFE;

    dima_countdown_t *countdown
            = dima_new_proxy(next, invoke_countdown, flags, sizeof(*countdown));
    if (ecb_expect_false(countdown == NULL)) {
        return NULL;
    }

    countdown->disabled = false;
    countdown->n_allocs = 0;
    return countdown;
}

dima_t *dima_from_countdown(dima_countdown_t *countdown) {
    return dima_from_proxy(countdown);
}

void dima_unref_countdown(dima_countdown_t *countdown) {
    dima_unref_proxy(countdown);
}

void dima_start_countdown(dima_countdown_t *countdown, unsigned n_allocs) {
    countdown->n_allocs = n_allocs;
}

void dima_enable_countdown(dima_countdown_t *countdown) {
    countdown->disabled = false;
}

void dima_disable_countdown(dima_countdown_t *countdown) {
    countdown->disabled = true;
}
