/*
 * Copyright (C) 2021-2022 Karl Linden <karl.j.linden@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>

#include <ecb.h>

#include <dima/exiting_on_failure.h>
#include <dima/failure_hook.h>

ecb_cold static void exit_on_failure(void *userdata) {
    int *status = userdata;
    exit(*status);
}

dima_t *dima_new_exiting_on_failure(dima_t *next, int status) {
    unsigned flags = dima_flags(next);
    flags |= DIMA_EXITS_ON_FAILURE;

    int *hook = dima_new_failure_hook(
            next, exit_on_failure, flags, sizeof(*hook));
    if (ecb_expect_false(hook == NULL)) {
        return NULL;
    }

    *hook = status;

    return dima_from_failure_hook(hook);
}
